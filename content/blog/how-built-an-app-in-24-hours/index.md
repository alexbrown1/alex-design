---
title: How I built an app in 24 Hours
date: '2019-01-01T22:12:03.284Z'
---

### How I built an app in 24 hours

December 30th, 2018: It was a cold night when I realized I hadn’t completed a
single side project in 2018. I decided that I would commit the next 24 hours to
design, building and launching an MVP in order to build at least one side
project this year.

I struggle a lot with [Makers
Paralysis](https://othmane.io/writing/ship_therapy_maker_paralysis_week_0). I
have 5 different ideas at any time that I want to build and I always want to get
it perfect on the first try. Because of this I end up never finishing or
launching a project because it isn’t “complete” or doesn’t seem ready.

Within the next 24 hours whatever I build I would force myself to deploy and
share it. Finished or not.

### Guidelines

To start the project I came up with a few guidelines:

* Design, build an launch a product in 24 hours
* Spend $0 (Excluding the domain)
* Build the project with Vue.js
* Get someone to use the product

### The Idea

My first step was to look through all of the many unused domains that I had
purchased this year. Finally, I settled with
[builtwithvue.com](https://www.builtwithvue.com).

The idea is that Built with Vue would serve as a directory or collection of Vue
apps from awesome creators around the world.

Here are some of the features I wanted to tackle:

* A user can submit their Vue.js project (✅)
* A user can see a list of Vue.js projects (✅)
* A user can tag a project (❌)
* A user can see how many views a project gets (❌)

*✅ = I was able to complete it within 24 hours*

*❌ = It didn’t go as planned.*

### The design

This is one of my favorite parts of building an app. I opened up a blank Sketch
document and got to work.

#### Logo

I started with a logo. I set out 30 mins for this. Here’s what I came up with:

![](https://cdn-images-1.medium.com/max/1600/1*SFJ_oaGLaFr5dfKDipfRWw@2x.jpeg)

I combined the Vue logo and the Microsoft medal emoji. Creative I know.

![](https://cdn-images-1.medium.com/max/1600/1*qNyP9dtGgQKONP7Akb7eTQ.png)

#### UI

Next, I got to work on the UI portion. This took me about an hour and a half. I
kept it sloppy there’s no need to make everything perfect. I am just using it as
a reference. I found a few colors that I liked and then started playing with
some layouts. Once I picked a layout I liked, I fleshed out the rest of the
design.

Like I said I kept this very sloppy and used this as a loose reference. The
finished version looks different to this.

![](https://cdn-images-1.medium.com/max/1600/1*IoOX0acKzCUPlX1YwlPP_w.png)

### The build

This part took up most of my time. In order to build the app within the next 24
hours, I wanted to leverage as many already built free solutions as I could. So
I leaned heavily on Google. Here is the stack I ended up with.

* [Vue.j](https://vuejs.org/)s — I couldn’t not use Vue for this project. I also
really like how fast and easy it is to get set up.
* [Google Sheets](http://sheets.google.com) — I used google sheets and sheets api
to store and retrieve the projects and information.
* [Google Forms ](http://forms.google.com)— Instead of building my own submission
form at first I leveraged Google Forms. The submissions are sent to a separate
google sheet where I can review and approve each submission.
* [Netlify ](https://www.netlify.com/)— This was my first time using Netlify. I
loved everything about this product. All static sites are free and are driven
off of git pushes.
* [Google Analytics](https://analytics.google.com/analytics/web/)- I wanted to
keep track of how many page visits I had and it is super easy to implement.
* [GitLab](https://gitlab.com) — I used Gitlab for my source control. Really enjoy
this product as well.

As always it took longer than planned to build out each feature. Especially when
it came to interacting with Google Sheets. I ended up completing 2/4 planned
features after a few hours. Here’s what the app ended up looking like:

![](https://cdn-images-1.medium.com/max/1600/1*Dk1G7Fhxp2zQoDDgKEeSXQ.png)

#### Keep pushing through

This was the most important part of this project. Whenever I ran into an issue
during the build I would get discouraged and feel like giving up or just
starting over. I struggle with this a lot and is the main reason I had not built
anything in 2018.

There were a few times especially as the deadline got closer and closer that I
got close to closing my computer and giving up. I had doubts and thoughts about
how it was incomplete and may not be ready to launch.

Luckily because of this goal I had set for myself it gave me that extra push I
needed. After about 22 hours I pushed my last push to Gitlab and clicked the new
post button on the Vue.js subreddit.

### The post

![](https://cdn-images-1.medium.com/max/1600/1*0yGoVWuhepiWORcw7wNqFg@2x.jpeg)

At about 10:30 pm a few hours before the ball dropped in Times Square to mark
the end of the year I launched [builtwithvue](https://www.builtwithvue.com) with
[this
post](https://www.reddit.com/r/vuejs/comments/abejsn/with_2_hours_left_in_2018_i_spent_the_last_24/).
I was extremely nervous. Would the site crash? Would people hate this? Or even
worse would I even get one submission?

After about 30 mins I received my first submission! Shout out to <br> [Todd
Baur](http://twitter.com/toadkicker/) and his project [Now
Serving](https://nowserving.us/?ref=builtwithvue.com)! This made the last 24
hours worth it.

### Now what?

I would recommend this exercise to anyone. This really helped me to just get
something out there. If I didn’t do this I probably would never have built this
site. It would have sat in my todo list forever.

If you have a Vue app that you’ve built it’d be awesome if you submit your
project! If you don’t have a Vue app check out some of the awesome apps that
have been built for inspiration. I’d love to hear any feedback!

I’ve also launched a blog at Alex.design! You can follow my next project
FeatureWatch. I plan on building out this project in the open and documenting
the whole process.
